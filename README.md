# Journaler

This is a Ruby on Rails journaling app with a MySQL backend.

The Rails code is in the journal_web_app directory. Go there and follow the barebones Rails README.

This app uses the Devise gem for authentication and registration.

Here are the features implemented so far:
 * Registration
 * Login
 * Edit User Info
 * Create New Journal Entries
 * See Existing Journal Entries

Future enhancements needed for Minimally Viable Product:

* Routes for deleting & editing journal entries.
* Paginated entries view.
* CSS UI polish. 
* A responsive (React or Vue) JS UI with REST API integration
* Added security to verify users can only access their own entries and not other user's entries.
* Feedback from clients on additional features for MVP launch.
* SEO code.
