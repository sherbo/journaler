class MenuController < ApplicationController
  before_action :authenticate_user!
  def index
    @entries = Entry.where("user_id = ?", current_user.id)
  end

  def save
    @entry = Entry.new

    @entry.title = params[:entry][:title]
    @entry.content = params[:entry][:content]
    @entry.user = current_user

    if @entry.save
      flash[:notice] = "Successfully saved! :)"
    else
      flash[:error] = "Error saving entry! :("
    end
    redirect_to "/menu"
  end
end
