class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  has_many :entries, dependent: :destroy

  # Will return an array of follows for the given user instance
  has_many :follows, foreign_key: :followed_id, class_name: "Follower"

  # Will return an array of users who follow the user instance
  has_many :followers, through: :follows, source: :follower
end
