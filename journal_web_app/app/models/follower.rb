class Follower < ApplicationRecord
  # The user giving the follow
  belongs_to :follow, foreign_key: :follower_id, class_name: "User"

  # The user being followed
  belongs_to :user, foreign_key: :followed_id, class_name: "User"
end
