#  README
---
* Ruby version
  * This app was developed with ruby 2.7.0
  * Rails 6.0.2
  
* MySQL 
    * This app assumes a MySQL DB for the dev backend


#  INSTALL YARN, RBENV, RUBY, AND BUNDLER
---
It's recommended to use rbenv to install the latest version of Ruby (2.7.0 at the time this was developed). Then install bundler.

rbenv install 2.7.0

gem install bundler

rbenv rehash

After that, run "bundle install" to install this app's dependencies. 

* Database migration
  * modify config/database.yml if needed
  * db:setup 
  * To create a new migration: <tt>rails generate migration <migration_name> </tt>
  * To run migrations: <tt>rake db:migrate</tt>

* Run the Rails server
	* rails s
	* The server is available at: http://0.0.0.0:3000/
