Rails.application.routes.draw do
  root to: "home#index"
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  devise_scope :user do
    get 'menu' => 'menu#index'

    authenticated :user do
      root 'menu#index', as: :authenticated_root
      post 'menu' => 'menu#save'
    end

    authenticated :unuser do
      root 'users/sessions#new', as: :unauthenticated_root
    end

  end
end
