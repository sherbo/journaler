class FollowersFks < ActiveRecord::Migration[6.0]
  def change
    add_index :followers, [:followed_id, :follower_id], unique: true
    add_reference :followers, :follower_id, foreign_key: { to_table: :users }
    add_reference :followers, :followed_id, foreign_key: { to_table: :users }
  end
end
