class CreateJournalEntries < ActiveRecord::Migration[6.0]
  def change
    create_table :entries do |t|
      t.text :content, null: false
      t.string :title, null: true
      t.bigint :user_id, null: false
      t.timestamps
    end
  end
end
